﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Object : MonoBehaviour
{
    [SerializeField]
    private ObjectData m_objectData = null;
    [SerializeField]
    private GameObject m_model = null;
    [SerializeField]
    private TextMeshProUGUI m_name = null;

    private Rigidbody m_objectRigidbody = null;
    private LineRenderer m_lineRenderer = null;
    private List<Rigidbody> m_bigObjects = null; //the Space objects that will affect the position of this object, to make it easier, we only take in account the "Big" objects

    private void Awake()
    {
        if (!this.m_objectData)
        {
            return;
        }

        this.transform.tag = this.m_objectData.GetObjectTag();
        m_model.GetComponent<Renderer>().material.mainTexture = this.m_objectData.GetObjectTexture();
        this.GetComponent<Rigidbody>().mass = this.m_objectData.GetObjectMass();
        this.transform.localScale = new Vector3(this.m_objectData.GetObjectSize(), this.m_objectData.GetObjectSize(), this.m_objectData.GetObjectSize());
    }

    private void Start()
    {
        this.m_objectRigidbody = this.GetComponent<Rigidbody>();
        this.m_lineRenderer = this.GetComponent<LineRenderer>();
        this.m_lineRenderer.enabled = false;
        this.m_bigObjects = Utils.FindRigidbodiesWithTags(new string[] {"Star", "Black Hole"});  //We find the rigidbodies for our big objects

        this.m_objectRigidbody.AddForce(this.transform.forward * this.m_objectData.GetKickOffForce()); //We give a kick-off force to our object, so it has a nice trajectory and does not "fall" to the big objects

        this.m_name.text = transform.name;
        this.m_name.enabled = false;
    }

    private void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * m_objectData.GetObjectRotation()); // Rotate the object along its up axis

        if(this.m_name.enabled == true)
        {
            this.m_name.transform.rotation = Quaternion.LookRotation(this.m_name.transform.position - Camera.main.transform.position); // Have the name of the planet always face the camera
        }
    }

    /*
     * Because we are doing the simulation based on physical equations, most the physics will be in here
     */
    private void FixedUpdate()
    {
        //We loop through all of our big objects and apply gravitational force accordingly
        foreach(Rigidbody bigObject in this.m_bigObjects)
        {
            if(bigObject == this.m_objectRigidbody) // We do not want an object to apply gravitational force to itself (dividing by 0, due to the distance part of the equation)
            {
                continue;
            }

            m_objectRigidbody.AddForce(NewtonForce(bigObject.position, this.m_objectRigidbody.position, bigObject.mass, this.m_objectRigidbody.mass), ForceMode.Force); // We apply the gravitational force between the two objects
        }

        if (m_lineRenderer.enabled) //Means that the object is selected
        {
            Vector3[] positions = GetTrajectory(this.m_objectRigidbody, this.m_objectData.GetOrbitSize()); //Get the trajectory
            //Set the points positions on the line renderer
            m_lineRenderer.positionCount = positions.Length;
            m_lineRenderer.SetPositions(positions);
        }
    }

    /*
     * Return an array of Vector3 that describes the future positions of the object
     * Basically, simulate multiple loops of FixedUpdate
     */
    private Vector3[] GetTrajectory(Rigidbody p_rigidbody, float p_duration)
    {
        int points = (int) (p_duration / Time.fixedDeltaTime); // Our sampling

        Vector3[] positions = new Vector3[points];
        Vector3[] velocities = new Vector3[points];

        positions[0] = p_rigidbody.position;
        velocities[0] = p_rigidbody.velocity;

        for(int i = 0; i < points - 1; i++)
        {
            Vector3 updatedVelocity = velocities[i];

            //We loop through all of our big objects and apply gravitational force accordingly
            foreach (Rigidbody bigObject in this.m_bigObjects)
            {
                if (bigObject == this.m_objectRigidbody)
                {
                    continue;
                }

                //We update the velocity, simulating a Newton Force on the rigidbody (In Force mode, the velocity of a rigidbody is given by the following equation : (force * Time.fixedDeltaTime) / rb.mass)
                updatedVelocity += (NewtonForce(bigObject.position, positions[i], bigObject.mass, p_rigidbody.mass) * Time.fixedDeltaTime) / p_rigidbody.mass;
            }

            velocities[i] = updatedVelocity;
            positions[i + 1] = positions[i] + (velocities[i] * Time.fixedDeltaTime); //Deducing the position from the velocity
            velocities[i + 1] = velocities[i];
        }

        return positions;
    }

    /*
     * Calculate a Newton force, given two positions and two masses
     */
    private Vector3 NewtonForce(Vector3 p_aPos, Vector3 p_bPos, float p_aMass, float p_bMass)
    {
        Vector3 direction = p_aPos - p_bPos;
        float force = 0.664f * (p_aMass * p_bMass) / Mathf.Pow(direction.magnitude, 2);

        return force * direction * m_objectData.GetObjectSpeed();
    }
}
