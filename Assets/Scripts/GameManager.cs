﻿using UnityEngine;
using TMPro;
using System;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_text = null;

    private float m_previousScale = 0.0f;
    private GameObject m_focusedObject = null;
    private GameObject[] m_planets = null;

    private string m_speeds = null;

    private void Start()
    {
        m_planets = GameObject.FindGameObjectsWithTag("Planet");
    }

    private void Update()
    {
        //Print the velocity of the planets
        foreach(GameObject planet in m_planets)
        {
            Rigidbody rb = planet.GetComponent<Rigidbody>();
            string tmp = planet.name + ": " + Math.Round(rb.velocity.magnitude, 0) + "; ";
            m_speeds += tmp;
        }

        m_text.text = m_speeds;

        m_speeds = null;

        if(Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            Time.timeScale *= 2;
        }
        else if(Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            Time.timeScale /= 2;
        }
        else if(Input.GetKeyDown(KeyCode.Space))
        {
            if(Time.timeScale != 0.0f)
            {
                m_previousScale = Time.timeScale;
                Time.timeScale = 0.0f;
            }
            else
            {
                Time.timeScale = m_previousScale;
            }
        }
        else if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit)) //If we hitted an object
            {
                if(m_focusedObject) //Unfocus the previous object
                {
                    m_focusedObject.GetComponent<LineRenderer>().enabled = false;
                    m_focusedObject.GetComponentInChildren<TextMeshProUGUI>().enabled = false;
                    m_focusedObject = null;
                }
                //Focus on the new object
                m_focusedObject = hit.collider.gameObject;
                m_focusedObject.GetComponent<LineRenderer>().enabled = true;
                m_focusedObject.GetComponent<LineRenderer>().positionCount = 0;
                m_focusedObject.GetComponent<AudioSource>().Play();
                m_focusedObject.GetComponentInChildren<TextMeshProUGUI>().enabled = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            //Unfocus the object
            if(m_focusedObject)
            {
                m_focusedObject.GetComponent<LineRenderer>().positionCount = 0;
                m_focusedObject.GetComponent<LineRenderer>().enabled = false;
                m_focusedObject.GetComponentInChildren<TextMeshProUGUI>().enabled = false;
                m_focusedObject = null;
            }
        }
    }
}
