﻿using System.Collections.Generic;
using UnityEngine;


/*
 * Different types of Space Objects
 */
enum e_spaceObjects
{
    PLANET,
    STAR,
    BLACK_HOLE,
    OTHER
};

class Utils
{
    /*
     * Will return a list of Rigidbodies, according to the tags given in parameters
     */
    static public List<Rigidbody> FindRigidbodiesWithTags(string[] p_tags)
    {
        List<Rigidbody> finalList = new List<Rigidbody>();
        GameObject[] objectsFound = null;

        foreach (string tag in p_tags)
        {
            objectsFound = GameObject.FindGameObjectsWithTag(tag);
            foreach(GameObject go in  objectsFound)
            {
                finalList.Add(go.GetComponent<Rigidbody>());
            }
        }

        return finalList;
    }
}
