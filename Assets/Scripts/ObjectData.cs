﻿using UnityEngine;

/*
 * This defines the properties of the different space objects (type, mass, speed, rotation rate...)
 */

[CreateAssetMenu(fileName = "New Space Object", menuName = "Space Object")]
public class ObjectData : ScriptableObject
{
    [SerializeField]
    private e_spaceObjects m_objectType = e_spaceObjects.OTHER;
    [SerializeField]
    private float m_objectMass = 0.0f;
    [SerializeField]
    private float m_objectSize = 0;
    [SerializeField]
    private float m_objectRotation = 0.0f;
    [SerializeField]
    private float m_kickOffForce = 0.0f;
    [SerializeField]
    private float m_orbitSize = 0.0f; //To have more or less points for trajectory prediction
    [SerializeField]
    private float m_objectSpeed = 0.0f;
    [SerializeField]
    private Texture2D m_objectTexture = null;

    public string GetObjectTag()
    {
        switch(this.m_objectType)
        {
            case e_spaceObjects.BLACK_HOLE:
                return "Black Hole";
            case e_spaceObjects.PLANET:
                return "Planet";
            case e_spaceObjects.STAR:
                return "Star";
            case e_spaceObjects.OTHER:
                return "Other Space Object";
            default:
                return "Untagged";
        }
    }

    public float GetObjectMass()
    {
        return this.m_objectMass;
    }

    public float GetObjectSize()
    {
        return this.m_objectSize;
    }

    public float GetObjectRotation()
    {
        return this.m_objectRotation;
    }

    public float GetKickOffForce()
    {
        return this.m_kickOffForce;
    }

    public float GetOrbitSize()
    {
        return this.m_orbitSize;
    }

    public float GetObjectSpeed()
    {
        return this.m_objectSpeed;
    }

    public Texture2D GetObjectTexture()
    {
        return this.m_objectTexture;
    }
}
